<?php

/* 
Plugin Name: ÉO Composer
Description: Visual Composer based on ACF
Version: 0.0.4
Author: Vincent Wasteels - étrangeOrdinaire
Author URI: http://etrangeordinaire.fr
Text Domain: eo-composer
*/

// @note : to update ACF.fields, 
// 1. comment this line
// 2. modify field via WP-ADMIN interface
// 3. export ACF.fields in PHP format and copy them in ./fields.php
// 4. uncomment this line
require_once(plugin_dir_path(__FILE__) . 'fields.php');


/**
 * Javascript
 */

add_action( 'wp_enqueue_scripts', function() {
	wp_register_script( 'jquery', plugins_url( '/js/jquery.min.js', __FILE__ ) );
	wp_enqueue_script( 'jquery' );

	wp_register_script( 'slick', plugins_url( '/js/slick.min.js', __FILE__ ) );
	wp_enqueue_script( 'slick' );

	wp_register_style( 'slick', plugins_url( '/css/slick.css', __FILE__ ), array());
    wp_enqueue_style( 'slick' );

	wp_register_script( 'eovc', plugins_url( '/js/eovc.js', __FILE__ ) );
	wp_enqueue_script( 'eovc' );

    wp_register_style( 'eovc', plugins_url( '/css/eovc.css', __FILE__ ), array());
    wp_enqueue_style( 'eovc' );

});



/*
 * Admin JS/CSS
 */

add_action('admin_head', function() {
  echo '<script type="text/javascript" src="'. plugins_url( '/js/eovc-admin.js', __FILE__ ) . '"></script>';
  echo '<link rel="stylesheet" href="'.        plugins_url( '/css/eovc-admin.css', __FILE__ ) . '" />';
});


/**
 * Template Tags
 */

function eovc() {
	global $post;
	global $col, $row, $rowSizes, $colKey;

	$rowSizes = [
		'4-8' => ['33.333','66.666'],
		'8-4' => ['66.666','33.333']
	];

	if(get_field('composer_composer')):
	?>
	<div class="eovc <?= get_field('composer_has_margin_top') ? '' : 'eovc-nomargin' ?>">
		<?
		foreach(get_field('composer_composer') as $rowKey => $row):
			
			$style = [];
			$style['background-color'] = 'centered'==$row['width']&&$row['bgcolor']?$row['bgcolor']:'transparent';
			$style['background-image'] = $row['bgimage'] ? 'url('.$row['bgimage']['sizes']['slider'].')' : 'none';
			$style['min-height'] = $row['min_height'] ? $row['min_height'].'px' : 'auto';
			$styleString = '';
			foreach($style as $key => $value) $styleString .= $key.':'.$value.';';

			if($centered == $row['width'] && $row['bgcolor'])
			?>
			<div class="eovc-row <?= $row['has_margin']?'has-margin':'' ?> eovc-row-<?= $row['width'] ?>" data-count="<?= count($row['columns']) ?>" style="<?= $styleString ?>">
				<div class="eovc-row-inner">
				<?
				if($row['columns']):
				foreach($row['columns'] as $colKey => $col):
					eovc_column();
				endforeach;
				endif;
				?>
				</div>
			</div>
			<?
		endforeach;
		?>
	</div>

	<div class="eovc-svgstore">
		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<symbol id="eovcicon-arrow" viewBox="0 0 20.7 15.5">
				<path d="M11.3,13.8c0-0.3,0.1-0.5,0.3-0.7l4-3.9H1.1C0.5,9.2,0,8.5,0,7.7
					c0-0.9,0.5-1.4,1.1-1.4h14.4l-4-3.9c-0.2-0.2-0.3-0.5-0.3-0.7C11.3,0.8,12,0,12.9,0c0.3,0,0.5,0.1,0.7,0.3l6.6,6.6
					c0.3,0.3,0.5,0.6,0.5,0.9c0,0.3-0.2,0.6-0.5,0.9l-6.6,6.6c-0.2,0.2-0.5,0.3-0.7,0.3C12.1,15.5,11.3,14.7,11.3,13.8z"/>
			</symbol>
			<symbol id="eovcicon-download" viewBox="-131.8 120.7 22 25.8">
				<path d="M-126.7,132c0.3,0,0.5,0.1,0.7,0.3l3.9,4v-14.5c0-0.6,0.7-1.1,1.5-1.1c0.9,0,1.4,0.5,1.4,1.1v14.4l3.9-4
					c0.2-0.2,0.5-0.3,0.7-0.3c0.9,0.1,1.7,0.8,1.7,1.7c0,0.3-0.1,0.5-0.3,0.7l-6.6,6.6c-0.3,0.3-0.6,0.5-0.9,0.5s-0.6-0.2-0.9-0.5
					l-6.6-6.6c-0.2-0.2-0.3-0.5-0.3-0.7C-128.4,132.8-127.6,132-126.7,132z"/>
				<path d="M-110.3,146.5h-21c-0.3,0-0.5-0.2-0.5-0.5v-9.8c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5v9.3h20v-9.3c0-0.3,0.2-0.5,0.5-0.5
					s0.5,0.2,0.5,0.5v9.8C-109.8,146.3-110,146.5-110.3,146.5z"/>
			</symbol>
		</svg>
	</div>
	<?
	endif;
}


function eovc_column() {
	global $col, $row, $rowSizes, $colKey;

	switch($row['structure']) {
		case 'even':
			$width = 100/count($row['columns']);
			break;
		default:
			$width = $rowSizes[$row['structure']][$colKey];
			break;
	}
	?>
	<div class="eovc-col" style="width:<?= $width ?>%;">
	<?
	switch($col['acf_fc_layout']) {
		
		case 'wysiwyg':

			$bgRgba = '';
			if($col['bgcolor']) {
				list($r, $g, $b) = sscanf($col['bgcolor'], "#%02x%02x%02x");
				$bgOpacity = $col['bgopacity'] / 100;
				$bgRgba = "rgba($r, $g, $b, ".$bgOpacity.")";
			}
			?>
			<div class="eovc-wysiwyg <?= $col['vertically_centered']?'vertically_centered':'' ?>" style="background-color:<?= $bgRgba ?>; color: <?= $col['textcolor'] ?>; background-image: url(<?= $col['bgimage']['sizes']['slider'] ?>)">
				<? if($col['title']): ?><h2 class="eovc-wysiwyg-title" style="border-color=<?= $col['textcolor'] ?>"><?= $col['title'] ?></h2><? endif; ?>
				<div class="eovc-wysiwyg-content"><?= apply_filters('the_content', $col['wysiwyg']) ?></div>
			</div>
			<?
			break;

		case 'slider':
			?>
			<div class="eovc-slider <?= $col['show_full']?'eovc-slider-full':'eovc-slider-cover' ?>" data-autoplay="<?= $col['autoplay'] ? 'true' : 'false' ?>" data-dots="false">
				<? 
				foreach($col['slider'] as $item):
					if($col['show_full']):
						?>
						<img class="eovc-slider-img" src="<?= $item['url'] ?>">
						<?
					else:
						?>
						<div class="eovc-slider-item" style="background-image:url(<?= $item['sizes']['slider'] ?>)"></div>
						<?
					endif;
				endforeach;
				?>
			</div>
			<?
			break;

		case 'video':
			?>
			<div class="eovc-video">
				<?= $col['video'] ?>
			</div>
			<?
			break;
	}
	?>
	</div>
	<?
}


/**
 * ShortCode : box-text
 * ex : [button bgcolor="#123456" txtcolor="#123456" icon="download|arrow" size="normal|large" url="http://url.com"]Cliquez-ici[/button]
 */

function eovcButton($atts, $content = null) {
  extract(shortcode_atts([
    'bgcolor' => '',
    'txtcolor' => '',
    'url' => '',
    'icon' => '',
    'size' => 'normal'
  ], $atts));
  ob_start();
  ?>
  <a class="eovc-button eovc-button-<?= $size ?>" target="_blank" href="<?= $url ?>" style="background-color:<?= $bgcolor ?>;color:<?= $txtcolor; ?>"><?= do_shortcode($content) ?><? if($icon) eovcIcon($icon, $txtcolor); ?></a>
  <?
  return ob_get_clean();
}


/**
 * Register shortcodes
 */

add_action( 'init', function() {
  add_shortcode('button', 'eovcButton');
});


/**
 * Icon
 */

function eovcIcon($icon, $txtcolor) {
	echo '<svg class="eovc-icon eovc-icon-'.$icon.'" style="fill:'.$txtcolor.'"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eovcicon-'.$icon.'"></use></svg>';
}
?>