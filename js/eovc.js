/* SLIDER */
/* --------------------------------------------------------------------------------- */

var isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);


jQuery(document).ready(function($) {

	

	$('.eovc-slider').each(function(key, element) {
		var $element = $(element);

		// init
		$element.slick({
			autoplay: $element.attr('data-autoplay') == 'false' ? false : true,
			autoplaySpeed: 5000,
			infinite: true,
			dots: false,
			arrows: true
		});
		
		// unset autoplay
		$element.on('click', function() {
			$element.slick('slickSetOption', 'autoplay', false);
		});

	});

	// @bug: safari gots problem with flex children's height
	if(isSafari) {
		$('.eovc-col').each(function(key, element) {
			var $element = $(element);
			if($element.find('.eovc-slider-full').length) return;
			console.log('allo');
			var rowHeight = $element.closest('.eovc-row-inner').outerHeight();
			$element.css('height', rowHeight);
		});
	}
});


