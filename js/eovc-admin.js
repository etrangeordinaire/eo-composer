/* ADMIN */
/* --------------------------------------------------------------------------------- */

jQuery(document).ready(function($){
	
	
	if($("body").is(".wp-admin, .post-type-projects")){
		
		// collapse row on click
		$(document).on('click', '.acf-row-handle, .row-name-inserted', function() {
			$(this).closest('.acf-row').toggleClass('collapsed');
		});

		// initial state
		$('.acf-row').addClass('collapsed');

		$('.row_name').each(function() {
			var el = this;
			updateRowName(el, $(el).find('input').val())

			$(el).find('input').on('change', function() {
				updateRowName(el, $(this).val())
			})
		});

		$('.acf-flexible-content .layout').addClass('-collapsed');
	}
});

function updateRowName(el, text) {
	var parent = jQuery(el).closest('.acf-fields');
	if(!parent.find('.row-name-inserted').length) parent.prepend('<div class="row-name-inserted"></div>');

	parent.find('.row-name-inserted').html(text);	

	
}