'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');
var rename = require('gulp-rename');

var execSync = require('child_process').execSync,
    commitHash = execSync('git log -n 1', {'encoding': 'UTF8'});

var paths = {
    sass: 'sass/{,*/}*.scss'
};

// append ?noCache=<hash-from-git-commit> on app.js and app.css files
var noCache = "?noCache=" + commitHash.split("\n")[0].split(" ")[1].substring(0,8);

gulp.task('sass', function() {
    return gulp.src('sass/index.scss')
        .pipe(sass({
            sourceComments: 'none'
        }))
        .on('error', onError)
        .pipe(prefix(['last 2 versions', 'ie >= 10']))
        .pipe(rename('eovc.css'))
        .pipe(gulp.dest('css'));
});


gulp.task('default', ['sass'], function() {
    gulp.watch(paths.sass, ['sass']);
});



/**
 * Utils.
 */

var gutil = require('gulp-util');
var notify = require('gulp-notify');
var isStream = require('isstream');
var os = require('os');

function onError() {
	var args = Array.prototype.slice.call(arguments);

	// error to notification center with gulp-notify
	if ('win32' != os.platform()) {
		notify.onError({
			title: "Compile Error",
			message: "<%= error.message %>"
		}).apply(this, args);
	}
	else {
		gutil.log("Compile Error", args[0].message, gutil.colors.red);
	}

	// keep gulp from hanging on this task
	if (isStream(this)) this.emit('end');
}